import express, { Application } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import { setupRoutes } from './src/routes/_index.route';
import { dbConfig } from './src/config/db.config';

require('dotenv/config');

const app: Application = express();

const PORT: string = process.env.PORT || '';

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('src/public'));

setupRoutes(app);
dbConfig();

app.listen(PORT, () => {
  console.log('Listening on port:', PORT);
});
 