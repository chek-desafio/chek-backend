import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export interface Token {
  uid: string;
  role: string;
  decoded: number;
  exp: number;
}

export const validateJWT = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {

  const token = req.header('token');
  const jwtSecretKey = process.env.JWT_SECRETKEY || '';

  if (!token) {
    return res.status(401).json({
      msg: 'No hay token en la petición',
    });
  }

  try {
    const { uid, role } = jwt.verify(token, jwtSecretKey) as Token;

    req.body.user = {
      uid,
      role,
    };

    next();
  } catch (error) {
    console.log(error);
    res.status(401).json({
      msg: 'Token no válido',
    });
  }
};
