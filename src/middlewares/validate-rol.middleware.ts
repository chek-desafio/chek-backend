import { Request, Response, NextFunction } from 'express';

export const isAdmin = (req: Request, res: Response, next: NextFunction) => {
  if (!req.body.user) {
    return res
      .status(500)
      .json({ message: 'Se debe verificar el token primero' });
  }

  const { uid, role } = req.body.user;
  if (role !== 'Admin') {
    return res
      .status(401)
      .json({ message: 'El usuario no tiene permisos de administrador' });
  }

  next();
};
