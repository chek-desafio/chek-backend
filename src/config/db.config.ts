import mongoose from 'mongoose';

export const dbConfig = async () => {
  try {
    mongoose.connect(process.env.DB_ADDRESS || '');
    console.log('Connected to db');
  } catch (error) {
    console.log('Error connecting to db', error);
  }
};
