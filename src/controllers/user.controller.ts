import { Request, Response } from 'express';
import * as userService from '../services/user.service';

export const getUsers = async (req: Request, res: Response) => {
  try {
    const users = await userService.getUsers();
    res.status(200).json({ ok: true, users });
  } catch (error: any) {
    res.status(500).json({ ok: false, message: error.message });
  }
};

export const getUser = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const user = await userService.getUser(id);
    res.status(200).json({ ok: true, user });
  } catch (error: any) {
    res.status(500).json({ ok: false, message: error.message });
  }
};
