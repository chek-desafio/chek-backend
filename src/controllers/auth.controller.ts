import { Request, Response } from 'express';
import * as authService from '../services/auth.service';

export const login = async (req: Request, res: Response) => {
  const { mail, password } = req.body;
  const lowerMail = mail.toLowerCase();

  try {
    const { user, token } = await authService.login(lowerMail, password);
    res.status(200).json({ ok: true, user, token });
  } catch (error: any) {
    let status = 400;

    if (
      error.message === 'Correo incorrecto' ||
      error.message === 'Contraseña incorrecta'
    ) {
      status = 400;
    } else if (error.message === 'Usuario inactivo') {
      status = 400;
    }

    res.status(status).json({ ok: false, message: error.message });
  }
};

export const register = async (req: Request, res: Response) => {
  const { name, password, rut, mail } = req.body;

  try {
    const lowerMail = mail.toLowerCase();
    const { client, token } = await authService.register(
      rut,
      lowerMail,
      name,
      password
    );

    res.status(200).json({ ok: true, user: client, token });
  } catch (error: any) {
    let status = 400;
    if (
      error.message ===
        `La cuenta con el rut: ${rut} ya se encuentra registrada.` ||
      error.message ===
        `La cuenta con el correo: ${mail} ya se encuentra registrada.`
    ) {
      status = 400;
    } else {
      status = 200;
    }

    res.status(status).json({ ok: false, message: error.message });
  }
};

export const verifyAccount = async (req: Request, res: Response) => {
  const { token } = req.params;

  try {
    const result = await authService.verifyAccountService(token);

    res.status(200).json({ ok: result.ok, message: result.message });
  } catch (error) {
    res.status(500).json({ ok: false, message: error });
  }
};

export const validateToken = async (req: Request, res: Response) => {
  const { token } = req.body;

  try {
    const result = await authService.validateTokenService(token);

    if (result) {
      res
        .status(200)
        .json({ ok: true, user: result.user, token: result.token });
    } else {
      res.status(404).json({ ok: false, message: 'Usuario no encontrado' });
    }
  } catch (error: any) {
    res.status(500).json({
      ok: false,
      message: error.message,
    });
  }
};
export const forgotPassword = async (req: Request, res: Response) => {
  const { mail } = req.body;

  try {
    const { status, response } = await authService.forgotPasswordService(mail);

    res.status(status).json(response);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const resetPassword = async (req: Request, res: Response) => {
  const { newPassword, verificationToken } = req.body;

  try {
    const { status, response } = await authService.resetPasswordService(
      newPassword,
      verificationToken
    );

    res.status(status).json(response);
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: false, message: 'Error en el servidor' });
  }
};

export const registerAdmin = async (req: Request, res: Response) => {
  const { name, password, mail, rut } = req.body;

  try {
    const admin = await authService.registerAdmin(name, password, mail, rut);

    res.status(200).json(admin);
  } catch (error) {
    res.status(500).json({ ok: false, message: `Error ${error}` });
  }
};
