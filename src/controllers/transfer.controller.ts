import { Request, Response } from 'express';
import * as transferService from '../services/transfer.service';

export const newTransfer = async (req: Request, res: Response) => {
  const { issuer, rutReceiver, amount, accountNumber, userMail } = req.body;

  try {
    const transfer = await transferService.createTransfer(
      issuer,
      rutReceiver,
      amount,
      accountNumber,
      userMail
    );
    
    res.status(200).json({ ok: true, transfer });
  } catch (error: any) {
    res.status(400).json({
      ok: false,
      message: error.message,
    });
  }
};

export const getUserTransfers = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const { combinedTransfers } = await transferService.getUserTransfers(id);
    res.status(200).json({ ok: true, transfers: combinedTransfers });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'No se ha podido obtener las transferencias del usuario',
    });
  }
};

export const getUsersTransfers = async (req: Request, res: Response) => {
  try {
    const transfers = await transferService.getAllTransfers();
    res.status(200).json({ ok: true, transfers });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'No se ha podido obtener las transferencias del usuario',
    });
  }
};
