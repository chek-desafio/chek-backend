import { Request, Response } from 'express';
import * as attemptService from '../services/login-attempt.service';

export const newLoginAttempt = async (req: Request, res: Response) => {
  const { mail, ipAddress, location, country } = req.body;
  const userAgent = req.headers['user-agent'];

  if (!mail || !ipAddress || !location || !country || !userAgent) {
    return res
      .status(400)
      .json({ ok: false, message: 'Todos los campos son requeridos' });
  }

  try {
    const loginAttempt = await attemptService.createLoginAttempt(
      mail,
      ipAddress,
      userAgent,
      location,
      country
    );
    res.status(200).json({ ok: true, loginAttempt });
  } catch (error: any) {
    res.status(500).json({ ok: false, message: error.message });
  }
};

export const getAllLoginAttempts = async (req: Request, res: Response) => {
  try {
    const attempts = await attemptService.getAllLoginAttempts();

    res.status(200).json({ ok: true, attempts });
  } catch (error) {
    res.status(500).json({ ok: false, message: 'Error interno del servidor' });
  }
};
