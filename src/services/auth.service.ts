import axios from 'axios';
import { generateJWT } from '../helpers/generate-jwt.helper';
import { Client } from '../models/client.model';
import { User } from '../models/user.model';
import bcrypt from 'bcrypt';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { Admin } from '../models/admin.model';

export const login = async (mail: string, password: string) => {
  const user = await User.findOne({ mail });

  if (!user) {
    throw new Error('Correo incorrecto');
  }
  if (!user.status) {
    throw new Error('Usuario inactivo');
  }

  let validPassword = false;
  if (user.password) {
    validPassword = bcrypt.compareSync(password, user.password);
  }
  if (!validPassword) {
    throw new Error('Contraseña incorrecta');
  }

  const token = await generateJWT(user.id, user.__t);

  return { user, token };
};

export const register = async (
  rut: string,
  mail: string,
  name: string,
  password: string
) => {
  const exists = await User.findOne({
    $or: [{ rut }, { mail }],
  });

  if (exists) {
    const errorMessage =
      exists.rut === rut
        ? `La cuenta con el rut: ${rut} ya se encuentra registrada.`
        : `La cuenta con el correo: ${mail} ya se encuentra registrada.`;
    throw new Error(errorMessage);
  }
  const encryptedPassword = await bcrypt.hash(password, 10);

  const client = await Client.create({
    name,
    password: encryptedPassword,
    rut,
    mail,
  });

  const token = await generateJWT(client.id);

  client.verificationToken = token as string;
  client.tokenPurpose = 'accountVerification';
  client.save();

  await axios.post(`https://mail.chek.fcidvidal.site/mail/verify`, {
    mail: mail,
    userName: name,
    token: token,
  });

  return { client, token };
};

export const verifyAccountService = async (token: string) => {
  const client = await User.findOne({ verificationToken: token });

  if (client && client.tokenPurpose === 'accountVerification') {
    client.status = true;
    client.verificationToken = '';
    client.tokenPurpose = '';

    await client.save();

    return { ok: true, message: 'Cuenta verificada exitosamente' };
  } else {
    return { ok: false, message: 'Token inválido' };
  }
};

export const validateTokenService = async (token: string) => {
  try {
    const jwtSecretKey = process.env.JWT_SECRETKEY || '';
    const { uid } = jwt.verify(token, jwtSecretKey) as JwtPayload;

    const user = await User.findById(uid);

    if (user) {
      const newToken = await generateJWT(user.id.toString());
      return { user, token: newToken };
    }

    return null;
  } catch (error) {
    throw new Error('Error al renovar token');
  }
};

export const forgotPasswordService = async (mail: string) => {
  if (!mail) {
    return {
      status: 400,
      response: { ok: false, message: 'El correo es requerido' },
    };
  }

  const client = await User.findOne({ mail });

  if (!client) {
    return {
      status: 400,
      response: {
        ok: false,
        message: 'No existe el usuario con el correo ' + mail,
      },
    };
  } else {
    const token = (await generateJWT(client.id)) as string;

    client.tokenPurpose = 'passwordReset';
    client.verificationToken = token;
    await client.save();

    await axios.post(`https://mail.chek.fcidvidal.site/mail/forgotPassword`, {
      mail: mail,
      userName: client.name,
      token: token,
    });

    return { status: 200, response: { ok: true, message: client.mail } };
  }
};

export const resetPasswordService = async (
  newPassword: string,
  verificationToken: string
) => {
  if (!(verificationToken && newPassword)) {
    return {
      status: 400,
      response: { ok: false, message: 'Todos los campos son requeridos' },
    };
  }

  const client = await User.findOne({ verificationToken: verificationToken });

  if (!client) {
    return { status: 400, response: { ok: false, message: 'Usuario no encontrado' } };
  }

  if (client && client.tokenPurpose === 'passwordReset') {
    const encryptedNewPassword = await bcrypt.hash(newPassword, 10);

    client.password = encryptedNewPassword;
    client.tokenPurpose = '';
    client.verificationToken = '';
    await client.save();

    return {
      status: 200,
      response: { ok: true, message: 'La contraseña fue cambiada con éxito' },
    };
  } else {
    return { status: 400, response: { ok: false, message: 'Propósito del token inválido' } };
  }
};

export const registerAdmin = async (
  name: string,
  password: string,
  mail: string,
  rut: string
) => {
  const encryptedPassword = await bcrypt.hash(password, 10);

  const admin = await Admin.create({
    rut,
    name,
    password: encryptedPassword,
    mail,
    status: true
  });

  return admin;
};
