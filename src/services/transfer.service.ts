import { Transfer } from '../models/transfer.model';
import { Client } from '../models/client.model';
import axios from 'axios';

export const createTransfer = async (
  issuer: string,
  rutReceiver: string,
  amount: number,
  accountNumber: number,
  userMail: string
) => {
  const clientIssuer = await Client.findById(issuer);

  const actualBalanceIssuer = clientIssuer!.balance;

  if (actualBalanceIssuer < amount) {
    throw new Error('Saldo inferior al monto a transferir');
  }

  const clientReceiver = await Client.findOne({ rut: rutReceiver });
  if (!clientReceiver) {
    throw new Error('El usuario no existe');
  }

  const idClientRecevier = clientReceiver!.id;

  if (clientReceiver!.rut.match(clientIssuer!.rut)) {
    throw new Error('No puedes transferirte a ti mismo');
  }

  clientReceiver!.balance += amount;
  clientIssuer!.balance -= amount;

  await clientReceiver!.save();
  await clientIssuer!.save();

  const transfer = await Transfer.create({
    issuer,
    receiver: idClientRecevier,
    amount,
  });

  await axios.post('https://mail.chek.fcidvidal.site/mail/newTransfer', {
    mail: [clientReceiver!.mail, userMail],
    userName: clientReceiver!.name,
    amount,
    date: transfer.createdAt,
    accountNumber,
  });

  return transfer;
};

export const getUserTransfers = async (id: string) => {
  const transfersIssued = await Transfer.find({ issuer: id })
    .sort({ createdAt: -1 })
    .populate('issuer', ['name'])
    .populate('receiver', ['name']);

  const transfersReceived = await Transfer.find({ receiver: id })
    .sort({ createdAt: -1 })
    .populate('receiver', ['name'])
    .populate('issuer', ['name']);

  const combinedTransfers = [
    ...transfersIssued.map((transfer) => ({
      ...transfer.toObject(),
      type: 'issued',
    })),
    ...transfersReceived.map((transfer) => ({
      ...transfer.toObject(),
      type: 'received',
    })),
  ];

  return { combinedTransfers };
};

export const getAllTransfers = async () => {
  const transfers = await Transfer.find();

  return transfers;
};
