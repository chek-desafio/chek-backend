import { User } from '../models/user.model';

export const getUsers = async () => {
  try {
    const users = await User.find();
    return users;
  } catch (error) {
    throw new Error('No se pudo obtener los usuarios: ' + error);
  }
};

export const getUser = async (id: string) => {
  try {
    const user = await User.findById(id);
    
    if (!user) {
      throw new Error('Usuario no encontrado');
    }

    return user;
  } catch (error) {
    throw new Error('No se pudo obtener al usuario');
  }
};
