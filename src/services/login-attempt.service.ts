import { LoginAttempt } from '../models/login-attempts.model';
import useragent from 'useragent';
import { User } from '../models/user.model';
import { Admin } from '../models/admin.model';

export const createLoginAttempt = async (
  mail: string,
  ipAddress: string,
  userAgent: string,
  location: string,
  country: string
) => {
  const agent = useragent.parse(userAgent);
  const browser = agent.toAgent();

  const client = await User.findOne({ mail });

  if (!client) {
    const admin = await Admin.findOne({ mail });

    if (!admin) {
      throw new Error('Usuario no existe');
    }

    const adminId = admin!.id;

    return await LoginAttempt.create({
      user: adminId,
      ipAddress,
      browser,
      location,
      country,
    });
  }

  const clientId = client!.id;

  return await LoginAttempt.create({
    user: clientId,
    ipAddress,
    browser,
    location,
    country,
  });
};

export const getAllLoginAttempts = async () => {
  return await LoginAttempt.find()
    .sort({ createdAt: -1 })
    .populate('user', ['name', 'mail']);
};
