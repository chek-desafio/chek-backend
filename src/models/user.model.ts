import moment from 'moment';
import { Model, Schema } from 'mongoose';
import mongoose from 'mongoose';

export interface User {
  name: string;
  rut: string;
  mail: string;
  status: boolean;
  password?: string;
  createdAt: Date;
  updatedAt: Date;
  tokenPurpose: string;
  verificationToken: string;
  __t: string;
}

const UserSchema = new Schema<User>({
  name: {
    type: String,
    required: [true, 'El nombre es requerido'],
  },
  mail: {
    type: String,
    required: [true, 'El correo electrónico es requerido'],
  },
  rut: {
    type: String,
    required: [true, 'El rut es requerido'],
  },
  status: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: () => moment.utc().toDate(),
  },
  updatedAt: {
    type: Date,
    default: () => moment.utc().toDate(),
  },
  tokenPurpose: {
    type: String,
  },
  verificationToken: {
    type: String,
  },
});

UserSchema.method('toJSON', function () {
  const { __v, _id, password, ...object } = this.toObject();
  object.uid = _id;
  return object;
});

export const User: Model<User> = mongoose.model('User', UserSchema);
