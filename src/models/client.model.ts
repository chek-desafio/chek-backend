import { Model, Schema } from 'mongoose';
import { User } from './user.model';

export interface Client extends User {
  password: string;
  balance: number; 
  __t: string;
}

const ClientSchema = new Schema<Client>({
  password: {
    type: String,
    required: [true, 'La contraseña es requerida'],
  },
  balance: {
    type: Number,
    default: 50000,
  },
});

ClientSchema.method('toJSON', function () {
  const { __v, __t, password, ...object } = this.toObject();
  object.role = __t;
  return object;
});

export const Client: Model<Client> = User.discriminator('Client', ClientSchema);
