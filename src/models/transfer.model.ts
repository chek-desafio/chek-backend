import { Model, Schema } from 'mongoose';
import mongoose from 'mongoose';
import { Client } from './client.model';
import moment from 'moment';
export interface Transfer {
  issuer: Client;
  receiver: Client;
  amount: number;
  createdAt: Date;
  updatedAt: Date;
}

const TransferSchema = new Schema<Transfer>({
  issuer: {
    type: mongoose.Types.ObjectId,
    ref: 'Client',
    required: [true, 'El emisor es requerido'],
  },
  receiver: {
    type: mongoose.Types.ObjectId,
    ref: 'Client',
    required: [true, 'El destinatario es requerido'],
  },
  amount: {
    type: Number,
    required: [true, 'El monto es requerido'],
  },
  createdAt: {
    type: Date,
    default: () => moment.utc().toDate(),
  },
  updatedAt: {
    type: Date,
    default: () => moment.utc().toDate(),
  },
});

TransferSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject();
  object.uid = _id;
  return object;
});

export const Transfer: Model<Transfer> = mongoose.model(
  'Transfer',
  TransferSchema
);
