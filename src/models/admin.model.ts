import { Model, Schema } from 'mongoose';
import { User } from './user.model';

export interface Admin extends User {
  password: string;
  __t: string;
}

const AdminSchema = new Schema<Admin>({
  password: {
    type: String,
    required: [true, 'La contraseña es requerida'],
  },
});

AdminSchema.method('toJSON', function () {
  const { __v, __t, password, ...object } = this.toObject();
  object.role = __t;
  return object;
});

export const Admin: Model<Admin> = User.discriminator('Admin', AdminSchema);
