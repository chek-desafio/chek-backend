import { Model, Schema } from 'mongoose';
import mongoose from 'mongoose';
import moment from 'moment';
import { User } from './user.model';

export interface LoginAttempt {
  user: User;
  ipAddress: string;
  browser: string;
  location: string;
  country: string;
  createdAt: Date;
}

const LoginAttemptSchema = new Schema<LoginAttempt>({
  user: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: [true, 'El usuario es requerido'],
  },
  ipAddress: {
    type: String,
    required: [true, 'La ip es requerida'],
  },
  browser: {
    type: String,
    required: [true, 'La info del browser es requerida'],
  },
  location: {
    type: String,
    required: [true, 'La localización es requerida'],
  },
  country: {
    type: String,
    required: [true, 'El país es requerido'],
  },
  createdAt: {
    type: Date,
    default: () => moment.utc().toDate(),
  },
});

LoginAttemptSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject();
  object.uid = _id;
  return object;
});

export const LoginAttempt: Model<LoginAttempt> = mongoose.model(
  'LoginAttempt',
  LoginAttemptSchema
);
