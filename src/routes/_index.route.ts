import { Application } from 'express';

import authRoutes from './auth.route';
import transferRoutes from './transfer.route';
import userRoutes from './user.route';
import attemptRoutes from './login-attempt.route';

export const setupRoutes = (app: Application) => {
  app.use('/auth', authRoutes);
  app.use('/transfer', transferRoutes);
  app.use('/user', userRoutes);
  app.use('/attempt', attemptRoutes);
};
