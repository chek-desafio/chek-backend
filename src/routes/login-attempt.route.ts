import { Router } from 'express';
import {
  getAllLoginAttempts,
  newLoginAttempt,
} from '../controllers/login-attempts.controller';
import { validateJWT } from '../middlewares/validate-jwt.middleware';
import { isAdmin } from '../middlewares/validate-rol.middleware';

const router = Router();

router.post('/', newLoginAttempt);
router.get('/', [validateJWT, isAdmin], getAllLoginAttempts);

export default router;
