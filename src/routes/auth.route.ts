import { Router } from 'express';
import {
  forgotPassword,
  login,
  register,
  registerAdmin,
  resetPassword,
  validateToken,
  verifyAccount,
} from '../controllers/auth.controller';
import { validateJWT } from '../middlewares/validate-jwt.middleware';

const router = Router();

router.post('/login', login);
router.post('/register', register);
router.post('/registerAdmin', registerAdmin);
router.post('/verifyAccount/:token', verifyAccount);
router.post('/forgotPassword', forgotPassword);
router.post('/resetPassword', resetPassword);
router.post('/validateToken', validateJWT, validateToken);

export default router;
