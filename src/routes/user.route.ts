import { Router } from 'express';
import { validateJWT } from '../middlewares/validate-jwt.middleware';
import { getUser, getUsers } from '../controllers/user.controller';
import { isAdmin } from '../middlewares/validate-rol.middleware';

const router = Router();

router.get('/', [validateJWT, isAdmin], getUsers);
router.get('/:id', validateJWT, getUser);

export default router;
