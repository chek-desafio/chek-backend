import { Router } from 'express';
import {
  newTransfer,
  getUserTransfers,
  getUsersTransfers,
} from '../controllers/transfer.controller';
import { validateJWT } from '../middlewares/validate-jwt.middleware';
import { isAdmin } from '../middlewares/validate-rol.middleware';

const router = Router();

router.post('/new', validateJWT, newTransfer);
router.get('/userTransfer/:id', validateJWT, getUserTransfers);
router.get('/userTransfer', [validateJWT, isAdmin], getUsersTransfers);

export default router;
