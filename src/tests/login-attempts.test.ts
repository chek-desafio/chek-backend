import { Client } from '../models/client.model';
import { LoginAttempt } from '../models/login-attempts.model';
import { User } from '../models/user.model';
import * as loginAttemptService from '../services/login-attempt.service';

import './test-setup';
import bcrypt from 'bcrypt';

const userRegistered = {
  mail: 'd.schrute@dundermifflin.com',
  name: 'Dwight Schrute',
  rut: '11111111-1',
  status: true,
  password: 'asd123',
};
const userRegistered2 = {
  mail: 'm.scott@dundermifflin.com',
  name: 'Michael Scott',
  rut: '222222222-2',
  status: true,
  password: 'asd123',
};

beforeEach(async () => {
  const encryptedPassword = await bcrypt.hash(userRegistered.password, 10);
  const encryptedPassword2 = await bcrypt.hash(userRegistered.password, 10);

  await Client.create({ ...userRegistered, password: encryptedPassword });
  await Client.create({ ...userRegistered2, password: encryptedPassword2 });

  const users = await User.find();
});

afterEach(async () => {
  await User.deleteMany({});
});

describe('New Login Attempt', () => {
  test('should create a login attempt', async () => {
    const existingUser = await User.findOne({ mail: userRegistered.mail });
    const ipAddress = '192.168.1.1';
    const userAgent = 'Chrome/ mobile';
    const location = 'Temuco Región de la Araucanía';
    const country = 'Chile';

    const loginAttempt = await loginAttemptService.createLoginAttempt(
      existingUser!.mail,
      ipAddress,
      userAgent,
      location,
      country
    );

    expect(loginAttempt).toBeDefined();
    expect(loginAttempt.ipAddress).toBe(ipAddress);
    expect(loginAttempt.browser).toBeDefined();
    expect(loginAttempt.location).toBe(location);
    expect(loginAttempt.country).toBe(country);
  });

  test('should throw an error if the email does not belong to a user or admin', async () => {
    const nonExistentEmail = 'j.halpert@dundermifflin.com';
    const ipAddress = '192.168.1.1';
    const userAgent = 'Chrome/ mobile';
    const location = 'Temuco Región de la Araucanía';
    const country = 'Chile';

    await expect(
      loginAttemptService.createLoginAttempt(
        nonExistentEmail,
        ipAddress,
        userAgent,
        location,
        country
      )
    ).rejects.toThrow('Usuario no existe');
  });
});

describe('New Login Attempt', () => {
  test('should retrieve all login attempts', async () => {
    const existingUser = await User.findOne({ mail: userRegistered.mail });
    const existingUser2 = await User.findOne({ mail: userRegistered2.mail });
    const ipAddress = '192.168.1.1';
    const location = 'Temuco Región de la Araucanía';
    const country = 'Chile';

    await LoginAttempt.create({
      user: existingUser!.id,
      ipAddress,
      browser: 'Chrome',
      location,
      country,
    });

    await LoginAttempt.create({
      user: existingUser2!.id,
      ipAddress,
      browser: 'Firefox',
      location,
      country,
    });

    const allLoginAttempts = await loginAttemptService.getAllLoginAttempts();

    expect(allLoginAttempts).toBeDefined();
    expect(Array.isArray(allLoginAttempts)).toBe(true);
    expect(allLoginAttempts.length).toBeGreaterThanOrEqual(2);
  });
});
