import { User } from '../models/user.model';
import bcrypt from 'bcrypt';

import * as authService from '../services/auth.service';

import './test-setup';
import { Client } from '../models/client.model';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { generateJWT } from '../helpers/generate-jwt.helper';

const userRegistered = {
  mail: 'd.schrute@dundermifflin.com',
  name: 'Dwight Schrute',
  rut: '11111111-1',
  status: true,
  password: 'asd123',
};

const userUnregistered = {
  mail: 'j.halpert@dundermifflin.com',
  name: 'Jim Halpert',
  status: true,
  rut: '22222222-2',
  password: 'asd123',
};

const userInactive = {
  mail: 'm.scott@dundermifflin.com',
  name: 'Michael Scott',
  rut: '33333333-3',
  password: 'asd123',
};

const userWithMail = {
  mail: 'p.beesly@dundermifflin.com',
  name: 'Pam Beesly',
  rut: '12222222-1',
  status: true,
  password: 'asd123',
};

const userWithoutMail = {
  mail: '',
  name: 'Jim Halpert',
  rut: '23333333-2',
  status: true,
  password: 'asd123',
};

function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

beforeEach(async () => {
  const encryptedPassword = await bcrypt.hash(userRegistered.password, 10);
  const encryptedPasswordInactive = await bcrypt.hash(
    userRegistered.password,
    10
  );
  const encryptedPasswordUserWithMail = await bcrypt.hash(
    userRegistered.password,
    10
  );
  await Client.create({ ...userRegistered, password: encryptedPassword });
  await Client.create({ ...userInactive, password: encryptedPasswordInactive });
  await Client.create({
    ...userWithMail,
    password: encryptedPasswordUserWithMail,
  });
  const users = await User.find();
});

afterEach(async () => {
  await User.deleteMany({});
});

describe('Login Service', () => {
  test('should login a user with correct email and password', async () => {
    await authService.login(userRegistered.mail, userRegistered.password);
  });

  test('should throw an error with incorrect email', async () => {
    await expect(
      authService.login('p.halpert@dundermifflin.com', 'asd123')
    ).rejects.toThrow('Correo incorrecto');
  });

  test('should throw an error with incorrect password', async () => {
    await expect(
      authService.login('d.schrute@dundermifflin.com', 'wrong_password')
    ).rejects.toThrow('Contraseña incorrecta');
  });

  test('should throw an error if user is inactive', async () => {
    await expect(
      authService.login('m.scott@dundermifflin.com', 'asd123')
    ).rejects.toThrow('Usuario inactivo');
  });
});

describe('Register Service', () => {
  test('should register a user', async () => {
    const userResponse = await authService.register(
      userUnregistered.rut,
      userUnregistered.mail,
      userUnregistered.name,
      userUnregistered.password
    );

    expect(userResponse).not.toBeNull();
  });

  test('should throw an error while register a user with the same rut', async () => {
    await expect(
      authService.register(
        userRegistered.rut,
        'f.cid04@ufromail.cl',
        'Francisco Cid',
        'asd123'
      )
    ).rejects.toThrow(
      `La cuenta con el rut: ${userRegistered.rut} ya se encuentra registrada.`
    );
  });

  test('should throw an error while register a user with the same mail', async () => {
    await expect(
      authService.register(
        '19519655-9',
        userRegistered.mail,
        'Francisco Cid',
        'asd123'
      )
    ).rejects.toThrow(
      `La cuenta con el correo: ${userRegistered.mail} ya se encuentra registrada.`
    );
  });
});

describe('Verify Account Service ', () => {
  test('should verify account and change status to true', async () => {
    const newUser = {
      mail: 'a.bernard@dundermifflin.com',
      name: 'Andy Bernard',
      rut: '12345678-9',
      password: 'asd123',
    };

    const registerResponse = await authService.register(
      newUser.rut,
      newUser.mail,
      newUser.name,
      newUser.password
    );

    const verificationToken = registerResponse.client.verificationToken;

    await expect(
      authService.verifyAccountService(verificationToken)
    ).resolves.toEqual({
      ok: true,
      message: 'Cuenta verificada exitosamente',
    });
  });

  test('should throw an error while verifying, due of invalid token', async () => {
    const invalidToken = 'invalid-token';

    await expect(
      authService.verifyAccountService(invalidToken)
    ).resolves.toEqual({ ok: false, message: 'Token inválido' });
  });
});

describe('Validate Token Service', () => {
  test('should validate a valid token', async () => {
    const newUser = {
      mail: 'c.bratton@dundermifflin.com',
      name: 'Creed Bratton',
      rut: '98765432-1',
      password: 'asd123',
    };

    const registerResponse = await authService.register(
      newUser.rut,
      newUser.mail,
      newUser.name,
      newUser.password
    );

    const verificationToken = registerResponse.client.verificationToken;

    await delay(300);
    const result = await authService.validateTokenService(verificationToken);

    const originalTokenPayload = jwt.decode(verificationToken) as JwtPayload;
    const newTokenPayload = jwt.decode(result!.token as string) as JwtPayload;

    expect(result).not.toBeNull();
    expect(result!.user).toBeDefined();
    expect(result!.token).toBeDefined();
    expect(newTokenPayload.iat).not.toBe(originalTokenPayload.iat);
  });

  test('should throw an error due invalid token', async () => {
    const invalidToken = 'invalid-token';

    await expect(
      authService.validateTokenService(invalidToken)
    ).rejects.toThrow('Error al renovar token');
  });
});

describe('Forgot Password Service', () => {
  test('should send reset token to existing user with valid email', async () => {
    const { status, response } = await authService.forgotPasswordService(
      userWithMail.mail
    );

    expect(status).toBe(200);
    expect(response).toEqual({
      ok: true,
      message: userWithMail.mail,
    });
  });

  test('should return an error for non-existing user', async () => {
    const nonExistentEmail = 'm.palmer@dundermifflin.com';
    const { status, response } = await authService.forgotPasswordService(
      nonExistentEmail
    );

    expect(status).toBe(400);
    expect(response).toEqual({
      ok: false,
      message: 'No existe el usuario con el correo ' + nonExistentEmail,
    });
  });

  test('should return an error if email is not provided', async () => {
    const { status, response } = await authService.forgotPasswordService(
      userWithoutMail.mail
    );

    expect(status).toBe(400);
    expect(response).toEqual({
      ok: false,
      message: 'El correo es requerido',
    });
  });
});

describe('Reset Password Service', () => {
  test('should return an error if newPassword or verificationToken are missing', async () => {
    const result = await authService.resetPasswordService('', '');
    expect(result.status).toBe(400);
    expect(result.response).toEqual({
      ok: false,
      message: 'Todos los campos son requeridos',
    });
  });

  test('should return an error if an invalid verificationToken is provided', async () => {
    const invalidToken = 'invalid-token';
    const newPassword = 'new-password';

    const result = await authService.resetPasswordService(
      newPassword,
      invalidToken
    );

    expect(result.status).toBe(400);
    expect(result.response).toEqual({
      ok: false,
      message: 'Usuario no encontrado',
    });
  });

  test('should reset password if a valid verificationToken and newPassword are provided', async () => {
    const registerResponse = await authService.register(
      '12345678-9',
      'g.lewis@dundermifflin.com',
      'Gabe Lewis',
      'asd123'
    );

    const passwordResetToken = (await generateJWT(
      registerResponse.client.id
    )) as string;

    registerResponse.client.tokenPurpose = 'passwordReset';
    registerResponse.client.verificationToken = passwordResetToken;
    registerResponse.client.status = true;
    await registerResponse.client.save();

    const newPassword = 'new-password';
    const result = await authService.resetPasswordService(
      newPassword,
      passwordResetToken
    );

    expect(result.status).toBe(200);

    expect(result.response).toEqual({
      ok: true,
      message: 'La contraseña fue cambiada con éxito',
    });

    const updatedUser = await authService.login(
      registerResponse.client.mail,
      newPassword
    );
    expect(updatedUser).toBeDefined();
  });
});

describe('Register Admin', () => {
  test('should register an admin with the provided data', async () => {
    const newAdmin = {
      rut: '12345678-9',
      mail: 'admin@test.com',
      name: 'Admin User',
      password: 'password',
    };

    const admin = await authService.registerAdmin(
      newAdmin.name,
      newAdmin.password,
      newAdmin.mail,
      newAdmin.rut
    );

    expect(admin).toBeDefined();
    expect(admin.rut).toBe(newAdmin.rut);
    expect(admin.mail).toBe(newAdmin.mail);
    expect(admin.name).toBe(newAdmin.name);
    expect(admin.password).not.toBe(newAdmin.password);
  });

  test('should store an encrypted password', async () => {
    const newAdmin = {
      rut: '23456789-0',
      mail: 'admin2@test.com',
      name: 'Admin User 2',
      password: 'password2',
    };

    const admin = await authService.registerAdmin(
      newAdmin.name,
      newAdmin.password,
      newAdmin.mail,
      newAdmin.rut
    );

    const passwordMatch = await bcrypt.compare(
      newAdmin.password,
      admin.password
    );
    expect(passwordMatch).toBe(true);
  });
});
