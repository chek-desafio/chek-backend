import { Client } from '../models/client.model';
import { Transfer } from '../models/transfer.model';
import { User } from '../models/user.model';
import * as transferService from '../services/transfer.service';

import './test-setup';
import bcrypt from 'bcrypt';

const userIssuer = {
  mail: 'd.schrute@dundermifflin.com',
  name: 'Dwight Schrute',
  rut: '11111111-1',
  status: true,
  password: 'asd123',
};
const userReceiver = {
  mail: 'm.scott@dundermifflin.com',
  name: 'Michael Scott',
  rut: '222222222-2',
  status: true,
  password: 'asd123',
};

beforeEach(async () => {
  const encryptedPasswordIssuer = await bcrypt.hash(userIssuer.password, 10);
  const encryptedPasswordReceiver = await bcrypt.hash(
    userReceiver.password,
    10
  );

  await Client.create({
    ...userIssuer,
    password: encryptedPasswordIssuer,
  });
  await Client.create({
    ...userReceiver,
    password: encryptedPasswordReceiver,
  });

  const users = await User.find();
});

afterEach(async () => {
  await User.deleteMany({});
  await Transfer.deleteMany({});
});

describe('New Transfer', () => {
  test('should create a transfer', async () => {
    const issuer = await Client.findOne({ mail: userIssuer.mail });
    const amount = 1000;
    const accountNumber = 123456;

    const transfer = await transferService.createTransfer(
      issuer!.id,
      userReceiver.rut,
      amount,
      accountNumber,
      userIssuer.mail
    );

    expect(transfer).toBeDefined();
    expect(transfer.issuer.toString()).toBe(issuer!.id);
    expect(transfer.amount).toBe(amount);
  });

  test('should throw an error if userReceiver is not found in db', async () => {
    const nonExistedRut = '19.519.655-9';
    const issuer = await Client.findOne({ mail: userIssuer.mail });
    const amount = 1000;
    const accountNumber = 123456;

    await expect(
      transferService.createTransfer(
        issuer!.id,
        nonExistedRut,
        amount,
        accountNumber,
        userIssuer.mail
      )
    ).rejects.toThrow('El usuario no existe');
  });

  test('should get user transfers', async () => {
    const issuer = await Client.findOne({ mail: userIssuer.mail });
    const receiver = await Client.findOne({ mail: userReceiver.mail });
    const amount = 1000;

    await Transfer.create({
      issuer: issuer!.id,
      receiver: receiver!.id,
      amount,
    });

    const userTransfers = await transferService.getUserTransfers(issuer!.id);

    expect(userTransfers).toBeDefined();
    expect(userTransfers.combinedTransfers.length).toBeGreaterThanOrEqual(1);
  });
  test('should get all transfers', async () => {
    const issuer = await Client.findOne({ mail: userIssuer.mail });
    const receiver = await Client.findOne({ mail: userReceiver.mail });
    const amount = 1000;

    await Transfer.create({
      issuer: issuer!.id,
      receiver: receiver!.id,
      amount,
    });

    const allTransfers = await transferService.getAllTransfers();

    expect(allTransfers).toBeDefined();
    expect(Array.isArray(allTransfers)).toBe(true);
    expect(allTransfers.length).toBeGreaterThanOrEqual(1);
  });
});
