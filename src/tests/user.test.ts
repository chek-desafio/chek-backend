import { User } from '../models/user.model';
import * as userService from '../services/user.service';

import './test-setup';

import bcrypt from 'bcrypt';

const userRegistered = {
  mail: 'userRegistered@example.com',
  name: 'User 1',
  rut: '11111111-1',
  status: true,
  password: 'asd123',
};

const userRegistered2 = {
  mail: 'user2@example.com',
  name: 'User 2',
  rut: '22222222-2',
  status: true,
  password: 'asd123',
};

beforeEach(async () => {
  const encryptedPassword1 = await bcrypt.hash(userRegistered.password, 10);
  const encryptedPassword2 = await bcrypt.hash(userRegistered2.password, 10);

  await User.create({ ...userRegistered, password: encryptedPassword1 });
  await User.create({ ...userRegistered2, password: encryptedPassword2 });

  const users = await User.find();
});

afterEach(async () => {
  await User.deleteMany({});
});

describe('User Service', () => {
  test('should get all users', async () => {
    const users = await userService.getUsers();

    expect(users).toBeDefined();
    expect(Array.isArray(users)).toBe(true);
    expect(users.length).toBeGreaterThanOrEqual(2);
  });

  test('should get only one user', async () => {
    const existingUser = await User.findOne({ mail: userRegistered.mail });

    const user = await userService.getUser(existingUser!.id);

    expect(user).toBeDefined();
    expect(user!.mail).toBe(userRegistered.mail);
    expect(user!.name).toBe(userRegistered.name);
  });

  test('should throw an error if the user is not found', async () => {
    const nonExistentUserId = '123456789012345678901234';

    await expect(userService.getUser(nonExistentUserId)).rejects.toThrow(
      'No se pudo obtener al usuario'
    );
  });
});
