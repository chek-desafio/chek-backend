FROM node:16-alpine 

# Create app directory
WORKDIR /app

COPY package*.json ./
COPY tsconfig.json ./

RUN npm install


EXPOSE 3000 
COPY . .


CMD ["npm", "run", "production"]
